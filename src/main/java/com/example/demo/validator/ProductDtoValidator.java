package com.example.demo.validator;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.enums.Category;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;


@Component
public class ProductDtoValidator {
    public void validate (ProductDto productDto, BindingResult bindingResult) {

        validateName(productDto, bindingResult);
        validatePrice(productDto, bindingResult);
        validateCategory(productDto,bindingResult);
        validateQuantity(productDto,bindingResult);

    }
    private void validateQuantity(ProductDto productDto, BindingResult bindingResult) {
        try {
            Integer quantity = Integer.valueOf(productDto.getQuantity());
            if (quantity <= 0) {
                FieldError fieldError = new FieldError("productDto", "quantity", "Quantity should be positive");
                bindingResult.addError(fieldError);
            }
        }
        catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("productDto", "quantity", "Quantity should be a numerical value.");
            bindingResult.addError(fieldError);
        }
    }
    private void validatePrice(ProductDto productDto, BindingResult bindingResult) {
        try {
            Integer price = Integer.valueOf(productDto.getPrice());
            if (price <= 0) {
                FieldError fieldError = new FieldError("productDto", "price", "Price should be positive");
                bindingResult.addError(fieldError);
            }
        }
        catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("productDto", "price", "Price should be a numerical value.");
            bindingResult.addError(fieldError);
        }
    }

    private void validateName(ProductDto productDto, BindingResult bindingResult) {
        if (productDto.getName().length() == 0){
            FieldError fieldError = new FieldError("productDto", "name", "Invalid name.");
            bindingResult.addError(fieldError);
        }

    }
    private void validateCategory(ProductDto productDto, BindingResult bindingResult) {
        try {
            Category category = Category.valueOf(productDto.getCategory());

        }
        catch (IllegalArgumentException exception) {
            FieldError fieldError = new FieldError("productDto", "category", "Invalid category.");
            bindingResult.addError(fieldError);
        }
    }


}
